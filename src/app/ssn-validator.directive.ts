import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS } from '@angular/forms';

function ssnValidator(control: FormControl) {
  const value = control.value || '';
  const valid = value.match(/^\d{3}$/) && value.length >= 3;
  return valid ? null : {ssn: true};
}

@Directive({
  selector: '[ssn]',
  providers: [{provide: NG_VALIDATORS, useValue: ssnValidator, multi: true}]
})

export class SsnValidatorDirective {}
