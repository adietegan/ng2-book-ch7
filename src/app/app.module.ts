import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SsnValidatorDirective } from './ssn-validator.directive';
import { SsnAsyncValidatorDirective } from "./async-ssn-validator.directive";

@NgModule({
  declarations: [
    AppComponent,
    SsnValidatorDirective,
    SsnAsyncValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
