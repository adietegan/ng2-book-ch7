import { Directive } from '@angular/core';
import { FormControl, NG_ASYNC_VALIDATORS } from '@angular/forms';
import { Observable } from "rxjs";

function asyncSsnValidator(control: FormControl) : Observable<any> {
  const value : string = control.value;
  const valid = value.match(/^\d{3}$/);
  return Observable.of(valid ? null : { ssn: true }).delay(5000);
}

@Directive({
  selector: '[ssn-async]',
  providers: [{
    provide: NG_ASYNC_VALIDATORS,
    useValue: asyncSsnValidator,
    multi: true
  }]
})

export class SsnAsyncValidatorDirective {}
