import { Component } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

function equalValidator({value}: FormGroup): {[key: string]: any} {
  const [first, ...rest] = Object.keys(value || {});
  const valid = rest.every(v => value[v] === value[first]);
  return valid ? null : {equal: true};
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app works!';

  formModel: FormGroup;

  constructor(fb: FormBuilder) {
    this.formModel = fb.group({
      'username': ['', [Validators.required, Validators.minLength(5)]],
      'ssn': [''],
      'passwordsGroup': fb.group({
        'password': ['', Validators.minLength(5)],
        'pConfirm': ['']
      }, {validator: equalValidator})
    });
  };

  onSubmit(value? : any, valid? : boolean) {
    console.log(value, valid);
    valid = valid || false;
    if (valid) {
      alert('meh, it\'s valid');
    }
  }
}
