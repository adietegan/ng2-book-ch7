import { Ng2BookCh7Page } from './app.po';

describe('ng2-book-ch7 App', function() {
  let page: Ng2BookCh7Page;

  beforeEach(() => {
    page = new Ng2BookCh7Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
